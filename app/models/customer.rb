class Customer < ActiveRecord::Base

  has_many :sales

  max_paginates_per 20

  def total_sales
    self.sales.map(&:amount_with_tax).sum
  end

end
